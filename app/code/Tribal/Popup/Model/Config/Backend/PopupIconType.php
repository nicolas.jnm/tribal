<?php

namespace Tribal\Popup\Model\Config\Backend;

class PopupIconType extends \Magento\Config\Model\Config\Backend\File
{
    /**
     * @return string[]
     */
    public function getAllowedExtensions()
    {
        return ['png', 'ico', 'svg'];
    }
}
