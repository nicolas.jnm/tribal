<?php

namespace Tribal\Popup\Block\Content;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use Tribal\Popup\Api\PopupInterface;
use Magento\Framework\UrlInterface;

class Popup extends Template
{

    private const BASE_PATH = 'header_tribal';

    private $_scopeConfigInterface;

    private $scopeStore = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

    protected $_storeManagerInterface;

    private $_popupInterface;

    const SUCCESS = 1;
    const INFO = 2;
    const WARNING = 3;
    const ERROR = 4;
    /**
     * __construct
     *
     * @return void
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManagerInterface,
        PopupInterface $popupInterface,
        array $data = []
    ) {
        $this->_scopeConfigInterface = $context->getScopeConfig();
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_popupInterface = $popupInterface;
        parent::__construct($context, $data);
    }

    /**
     * isActive
     *
     * @return void
     */
    public function isActive()
    {
        return $this->getConfig('popup/enable');
    }


    /**
     * getPopupType
     *
     * @return void
     */
    public function getPopupType()
    {
        $config = $this->getConfig('popup/popup_type');
        return $this->getPopupTypeClass($config);
    }

    /**
     * getPopupIcon
     *
     * @return array $type
     */
    public function getPopupTypeClass($type)
    {
        switch ($type) {
            case self::SUCCESS:
                $icon = $this->getConfig('popup/popup_type_success_icon');
                if (empty($icon)) {
                    return null;
                }
                return [
                    'class' => 'success',
                    'icon' => $this->mediaUrl(UrlInterface::URL_TYPE_MEDIA) . 'popup_type_success_icon/' . $icon
                ];
            case self::INFO:
                $icon = $this->getConfig('popup/popup_type_info_icon');
                if (empty($icon)) {
                    return null;
                }
                return [
                    'class' => 'info',
                    'icon' => $this->mediaUrl(UrlInterface::URL_TYPE_MEDIA) . 'popup_type_info_icon/' . $icon
                ];
            case self::WARNING:
                $icon = $this->getConfig('popup/popup_type_warning_icon');
                if (empty($icon)) {
                    return null;
                }
                return [
                    'class' => 'warning',
                    'icon' => $this->mediaUrl(UrlInterface::URL_TYPE_MEDIA) . 'popup_type_warning_icon/' . $icon
                ];
            case self::ERROR:
                $icon = $this->getConfig('popup/popup_type_error_icon');
                if (empty($icon)) {
                    return null;
                }
                return [
                    'class' => 'error',
                    'icon' => $this->mediaUrl(UrlInterface::URL_TYPE_MEDIA) . 'popup_type_error_icon/' . $icon
                ];
        }
    }

    /**
     * getInitialDate
     *
     * @return void
     */
    public function getInitialDate()
    {
        return $this->getConfig('popup/initial_date');
    }

    /**
     * getEndingDate
     *
     * @return void
     */
    public function getEndingDate()
    {
        return $this->getConfig('popup/ending_date');
    }

    /**
     * getTitle
     *
     * @return void
     */
    public function getTitle()
    {
        return $this->getConfig('popup/title');
    }

    /**
     * getPopupContent
     *
     * @return void
     */
    public function getPopupContent()
    {
        return $this->getConfig('popup/popup_content');
    }

    /**
     * getEnableExternalUrl
     *
     * @return void
     */
    public function getEnableExternalUrl()
    {
        return $this->getConfig('popup/enable_external_url');
    }

    /**
     * getExternalUrl
     *
     * @return void
     */
    public function getExternalUrl()
    {
        return $this->getConfig('popup/external_url');
    }

    /**
     * getExternalUrlLabel
     *
     * @return void
     */
    public function getExternalUrlLabel()
    {
        return $this->getConfig('popup/external_url_label');
    }


    /**
     * checkPopupCookie
     *
     * @return void
     */
    public function checkPopupCookie()
    {

        $currentCookie = $this->_popupInterface->get();

        $contentHash = hash('md5', $this->getPopupContent());

        if (($currentCookie !== $contentHash) && $this->validatePopupDate()) {
            return [
                'hash' => $contentHash,
                'cookie' => $currentCookie,
                'closeUrl' => $this->mediaUrl(UrlInterface::URL_TYPE_WEB) . 'popup/popup/set?hash=' . $contentHash,
            ];
        }

        return null;
    }

    /**
     * validatePopupDate
     *
     * @return void
     */
    public function validatePopupDate()
    {
        $currentDate = date('Y-m-d');
        $currentDate = date('Y-m-d', strtotime($currentDate));
        $initialDate = date('Y-m-d', strtotime($this->getInitialDate()));
        $endingDate = date('Y-m-d', strtotime($this->getEndingDate()));

        if (($currentDate >= $initialDate) && ($currentDate <= $endingDate)) {
            return true;
        }

        return false;
    }

    /**
     * setNewCookie
     *
     * @param  mixed $contentHash
     *
     * @return void
     */
    public function setNewCookie($contentHash)
    {
        return $this->_popupInterface->set($contentHash, 86400);
    }

    /**
     * getConfig
     *
     * @param  mixed $key
     *
     * @return void
     */
    public function getConfig($key)
    {
        return $this->_scopeConfigInterface->getValue(self::BASE_PATH . '/' . $key, $this->scopeStore);
    }

    /**
     * mediaUrl
     *
     * @return void
     */
    private function mediaUrl($type)
    {
        $currentStore = $this->_storeManagerInterface->getStore()->getBaseUrl($type);

        return $currentStore;
    }
}
