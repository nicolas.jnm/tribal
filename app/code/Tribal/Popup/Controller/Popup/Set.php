<?php

namespace Tribal\Popup\Controller\Popup;

use Magento\Framework\App\Action\Context;
use Tribal\Popup\Cookie\Popup;
use Tribal\Popup\Api\PopupInterface;
use Magento\Framework\App\RequestInterface;

class Set extends \Magento\Framework\App\Action\Action
{
    protected $_popupInterface;

    protected $_requestInterface;

    /**
     * __construct
     *
     * @return void
     */
    public function __construct(
        Context $context,
        PopupInterface $popupInterface,
        RequestInterface $requestInterface
    ) {
        $this->_popupInterface = $popupInterface;
        $this->_requestInterface = $requestInterface;
        parent::__construct($context);
    }

    /**
     * execute
     *
     * @return void
     */
    public function execute()
    {
        $hash = $this->_requestInterface->getParam('hash');

        $this->_popupInterface->set($hash, 86400);
    }
}
